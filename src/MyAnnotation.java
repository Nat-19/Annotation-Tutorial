import java.lang.annotation.*;

//Creo una Annotation utilizzabile per le classi(ElementType.TYPE)
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@interface MyFirstAnnotation {

	String name() default "Natale";
	int age();
	
}

//Creo una Annotation utilizzabile per le classi(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@interface MySecondAnnotation{
	String name() default "Giuseppe";
}

//Creo una Annotation utilizzabile per le classi(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
@interface MyThirdAnnotation{
	String name() default "Maria";
}
