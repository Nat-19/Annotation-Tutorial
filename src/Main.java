import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

public class Main {
	
	public static void main(String [] args) throws NoSuchMethodException, SecurityException, NoSuchFieldException{

		//Gestione class annotation
		MyClass myClass = new MyClass();
		
		//Creo gli oggetti da cui prelevvare le Annotation
		Class c = myClass.getClass();
		Method m = c.getMethod("doStuff", null); //indica nome e args del metodo 
		Field f = c.getField("x");
		
		//Prelevo le Annnotation che mi interessano e le assegno al proprio tipo di reference
		MyFirstAnnotation myFirstAnnotation = (MyFirstAnnotation)c.getAnnotation(MyFirstAnnotation.class);
		MySecondAnnotation mySecondAnnotation = (MySecondAnnotation)m.getAnnotation(MySecondAnnotation.class);
		MyThirdAnnotation myThirdAnnotation = (MyThirdAnnotation)f.getAnnotation(MyThirdAnnotation.class);
		
		
		//Mi prelevo i campi che mi interessano dalle varie annotation
		System.out.println(myFirstAnnotation.age());
		System.out.println(mySecondAnnotation.name());
		System.out.println(myThirdAnnotation.toString());
		
	}
	
	
	
}
